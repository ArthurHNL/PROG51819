﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Calculator
{
    class Calculator : IAgeCalculator
    {
        public String Format => "dd-mm-yyyy";
        public CultureInfo CurrentCulture => CultureInfo.CurrentCulture;

        public int CalculateAge(DateTime dateOfBirth)
        {
            TimeSpan diff = DateTime.Now.Subtract(dateOfBirth);
            DateTime convert = DateTime.MinValue + diff;
            //Subtract 1 becuase minvalue is 1 year.
            return convert.Year - 1;
        }

        public bool ParseInput(string input, out DateTime result)
        {
            if (input == null)
            {
                result = DateTime.MinValue;
                return false;
            }
            try
            {
                result = DateTime.ParseExact(input, Format, CurrentCulture);
                return true;
            } catch (FormatException)
            {
                result = DateTime.MinValue;
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator c = new Calculator();
            int exitCode;

            Console.WriteLine("Please enter your date of birth in the format of dd-mm-yyyy...");
            string input = Console.ReadLine();
            Console.WriteLine();
            
            if (c.ParseInput(input, out DateTime res))
            {
                int age = c.CalculateAge(res);
                Console.WriteLine("You are {0} years old.", age);
                exitCode = 0;
            } else
            {
                Console.WriteLine("Invalid input!");
                exitCode = -1;
            }
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Environment.Exit(exitCode);
        }
    }
}

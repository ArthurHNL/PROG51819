﻿using System;

namespace HelloLibrary
{
    public static class HelloWriter
    {

        public static void WriteHello(String name)
        {
            if (name == null)
            {
                name = "World";
            }

            Console.WriteLine("Hello {0}!", name);
        }

    }
}

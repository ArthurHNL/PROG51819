﻿using System;
using System.Collections.Generic;
using System.Text;
using HelloLibrary;

namespace HelloWorld
{
    class Program
    {

        public static void Main(String[] args)
        {

            Console.Write("Please enter your name... ");
            String name = Console.ReadLine();
            Console.WriteLine();
            HelloWriter.WriteHello(name);
            Console.ReadKey();
            Environment.Exit(0);

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phonebook.LIB.Models
{
    public class Entry
    {

        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String FullName => $"{FirstName} {LastName}";
        public String PhoneNumber { get; set; }

        public override String ToString()
        {
            return ($"{FullName} - {PhoneNumber}");
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace Phonebook.LIB.Models
{
    public class Phonebook
    {
        public List<Entry> Entries { get; private set; }

        public Phonebook() : this(null) { }
        public int Size => Entries.Count;

        public Phonebook(ICollection<Entry> initialEntries)
        {
            if (initialEntries != null)
            {
                Entries = new List<Entry>(initialEntries.Count);
                Entries.AddRange(initialEntries);
            }
            else
            {
                Entries = new List<Entry>();
            }
        }
        public List<Entry> GetSortedByLastName()
        {
            IOrderedEnumerable<Entry> res = Entries.OrderBy(e => e.LastName);
            return res.ToList();
        }
        public List<Entry> GetFirstnameBeginningWith(char beginningWith)
        {
            var res = GetSortedByLastName().Where(e => e.FirstName.StartsWith(beginningWith));
            return res.ToList();
        }
        public List<Entry> GetWhereLastNameGreatherThen(int value)
        {
            var res = GetSortedByLastName().Where(e => e.LastName.Length > value);
            return res.ToList();
        }
        public List<Entry> GetSortedByLastNameLength()
        {
            var res = Entries.OrderByDescending(e => e.LastName.Length);
            return res.ToList(); ;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BankingApplet
{
    class AddMoneyCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private PersonVM _context;

        public AddMoneyCommand(PersonVM context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _context.Balance += 10;
        }
    }
}

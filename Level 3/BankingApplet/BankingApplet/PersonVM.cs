﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApplet
{
    class PersonVM : INotifyPropertyChanged
    {
        public PersonVM()
        {
            DateOfBirth = new DateTime(1980, 1, 1);
            Balance = 100;
            AddMoneyCommand = new AddMoneyCommand(this);
            RemoveMoneyCommand = new RemoveMoneyCommand(this);
        }

        public AddMoneyCommand AddMoneyCommand { get; private set; }
        public RemoveMoneyCommand RemoveMoneyCommand { get; private set; }

        private String _name;
        public String Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                RaisePropertyChanged("Name");
            }
        }


        private DateTime _dob;
        public DateTime DateOfBirth
        {
            get
            {
                return _dob;
            }
            set
            {
                _dob = value;
                RaisePropertyChanged("DateOfBirth");
            }
        }

        private int _balance;

        public int Balance {
            get
            {
                return _balance;
            }
            set
            {
                _balance = value;
                RaisePropertyChanged("Balance");
            }
        }


        void RaisePropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

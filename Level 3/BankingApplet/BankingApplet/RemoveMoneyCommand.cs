﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BankingApplet
{
    class RemoveMoneyCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        private PersonVM _context;

        public RemoveMoneyCommand(PersonVM context)
        {
            _context = context;
        }

        public bool CanExecute(object parameter)
        {
            return (_context.Balance >= 10);
        }

        public void Execute(object parameter)
        {
            _context.Balance -= 10;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CommandingAndBindingDemo
{
    public class CelciusToFahrenheitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double celcius = (double)value;
            double fahrenheit = Math.Round(celcius * 1.8 + 32, 0);
            return fahrenheit;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double fahrenheit = (double)value;
            double celcius = Math.Round(fahrenheit / 1.8 - 32, 0);
            return celcius;
        }
    }
}

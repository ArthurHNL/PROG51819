﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.ViewModel;

namespace Inventory.Model
{
    public class DummyItemRepository
    {
        private List<Item> _items;
        
        public DummyItemRepository()
        {
            _items = new List<Item>();
            
            _items.Add(new Item("1"));
            _items.Add(new Item("2"));
            _items.Add(new Item("3"));
            _items.Add(new Item("4"));
            _items.Add(new Item("5"));
            _items.Add(new Item("6"));
            _items.Add(new Item("7"));
            _items.Add(new Item("8"));
            _items.Add(new Item("9"));
            _items.Add(new Item("0"));
        }

        public ObservableCollection<ItemViewModel> GetItems()
        {
            var items = _items.Select(i => new ItemViewModel() { Content = i.Content }).ToList();
            return new ObservableCollection<ItemViewModel>(items);
        }
    }
}

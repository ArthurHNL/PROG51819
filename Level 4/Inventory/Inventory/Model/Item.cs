﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Model
{
    public class Item
    {
        public string Content { get; set; }
        public Item(String content)
        {
            Content = content;
        }
    }
}

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Inventory.Model;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;

namespace Inventory.ViewModel
{

    public class MainViewModel : ViewModelBase
    {
        private DummyItemRepository _repo;

        public ObservableCollection<ItemViewModel> Items { get; set; }
        public ObservableCollection<ItemViewModel> BoughtItems { get; set; }
        public ItemViewModel Selected { get; set; }
        public ItemViewModel SelectedSell { get; set; }

        public ICommand BuyItemCommand { get; set; }
        public ICommand SellItemCommand { get; set; }

        public MainViewModel()
        {
            _repo = new DummyItemRepository();
            Items = _repo.GetItems();
            BoughtItems = new ObservableCollection<ItemViewModel>();
            BuyItemCommand = new RelayCommand(BuyItem, CanBuyItem);
            SellItemCommand = new RelayCommand(SellItem);
        }

        private void BuyItem()
        {

            BoughtItems.Add(Selected);
            Items.Remove(Selected);

        }
        private void SellItem()
        {

            Items.Add(SelectedSell);
            BoughtItems.Remove(SelectedSell);

        }
        private bool CanBuyItem()
        {
            return BoughtItems.Count < 6;
        }
    }
}